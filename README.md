# InvisibleAR

**Swift Package for AR**

*InvisibleAR helps developer to implement AR environment easily with invisible codes in the package*

- - -

## Copyright

- © 2019 Jaesung. All Rights Reserved.

- - -

## Supporting

### Supported Platform: iOS 11.0 or later

### Versions of Swift: Swift 4.3 or later


- - -

## Contact

### Jaesung
chic0815@icloud.com

- - -

## Feature


### Gesture

#### Tap

`tap(_:action:)`

```Swift
@objc func tapGesture(_ gesture: UITapGestureRecognizer) {
    Gesture.tap(gesture) { node in
        node.action(forKey: "Compress")
    }
}
```

#### Pan

`pan(_:)`

```Swift
@objc func panGesture(_ gesture: UIPanGestureRecognizer) {
    Gesture.pan(gesture, action: nil)
}
```

### Model
`init(withName:withKey:from:)`

`loadAnimation(from:)`

`loadAnimations(for:with:from:)`

`node(withName:from:)`

`animate(forKey:)`

`animate(with:)`


### Extensions

#### SCNNode

`scaling(value:)`

`scaling(x:y:z:)`

`position(x:y:z:)`

`animate(forKey:repeatCount:)`

`animate(with:)`

`tap(_:action:)`

`move(_:action:)`

#### SCNScene

`addModel(_:result:)`

`addNode(_:result:)`

#### ARSCNView

`createModel(_:at:with:)`

`createNode(_:at:with:)`


### Protocols

#### PlaneSpace
`showPlaneSpace(_:)`

- - -

## Version History

### 1.0.1
> Nov 16, 2019

### 1.0.0
> Nov 16, 2019

