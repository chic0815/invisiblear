import XCTest

import InvisibleARTests

var tests = [XCTestCaseEntry]()
tests += InvisibleARTests.allTests()
XCTMain(tests)
