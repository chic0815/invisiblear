//
//  Gesture.swift
//  
//
//  Created by Jaesung on 2019/11/13.
//

import ARKit

/**
 This class recognizes UIGestures in ARView.
 
 - Important: `@available(iOS 11.0, *)`
 
 - Supported Gesture: `tap()`, `pan()`
 */
@available(iOS 11.0, *)
public class Gesture {
    /**
    This method is called when you tapped `SCNNode` object in AR
     
    - Important: A gesture recognizer has one or more target-action pairs associated with it.
    
    - parameters:
       - gesture: `UIPanGestureRecognizer` object
       - action: The action that is applied to target(`SCNNode`)
    
    - Examle:
       ```Swift
       @objc func tapGesture(_ gesture: UITapGestureRecognizer) {
           Gesture.tap(gesture) { node in
               node.action(forKey: "Compress")
           }
       }
       ```
    */
    static func tap(_ gesture: UITapGestureRecognizer, action: Action) {
        guard let view = gesture.view as? ARSCNView else { return }
        
        let hit = view.hitTest(gesture.location(in: gesture.view), options: nil)
        
        guard let target = hit.first?.node else { return }
        
        action(target)
    }
    
    
    /**
     This method is called when you panned `SCNNode` object in AR
     
     - Important: A gesture recognizer has one or more target-action pairs associated with it.
     
     - parameters:
        - gesture: `UIPanGestureRecognizer` object
        - action: The action that is applied to target(`SCNNode`). It is `Optional`
     
     - Examle:
        ```Swift
        @objc func panGesture(_ gesture: UIPanGestureRecognizer) {
            Gesture.pan(gesture, action: nil)
        }
        ```
     */
    static func pan(_ gesture: UIPanGestureRecognizer, action: Action?) {
        gesture.minimumNumberOfTouches = 1
        
        guard let view = gesture.view as? ARSCNView else { return }
        let hit = view.hitTest(gesture.location(in: view), options: nil)
        
        guard let target = hit.first else { return }
        
        let hitWithPan = view.hitTest(gesture.location(in: gesture.view), types: .existingPlaneUsingExtent)
        guard let firstHWP = hitWithPan.first else { return }
        
        let vector = firstHWP.worldTransform.columns.3
        let position = SCNVector3Make(vector.x, vector.y, vector.z)
        
        target.node.position = position
        
        guard let action = action else { return }
        action(target.node)
    }
}
