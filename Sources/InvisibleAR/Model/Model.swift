//
//  Model.swift
//  
//
//  Created by 이재성 on 2019/11/13.
//

import SceneKit

/**
This class has one animating `SCNNode` object named `node` and a various node with no animating.
 
- Since: 1.0.0
*/
@available(iOS 11.0, *)
public class Model {
    // MArk: - Properties
    /**
     `SCNNode` object.  One `Model` Class has one animating `SCNNode` named node.
     
     - Important:
        Because `Model` class can have only one animating node, if you want to create node without animation, use `node(withName:from:)` method.
     
     - Since: 1.0.0
     */
    var node: SCNNode?
    

    /**
    Initializes with specific node from `SCNScene` name.
    
    - parameters:
       - withName: The node name you want to return. This is not a root node of `SCNScene`.
       - withKey: The animation key. This is optional. You don't have animation player, this value must be `'nil`
       - sceneName: The name of `SCNScene` you want to access
    
    - Returns: `SCNNode` object or `nil`.  If it is `nil`, please check parameter.
    
    - Since: 1.0.0
    */
    public init(withName: String, withKey: String?, from sceneName: String) {
        node = self.node(withName: withName, from: sceneName)
        guard let node = node else { return }
        if let key = withKey {
            loadAnimations(for: node, with: key, from: sceneName)
        }
        
    }
    
    // MARK: - Load Animations
    /**
     Loads animation  from scene.  Returned animation player can play the animation.
     
     - parameters:
        - sceneName: The scene name that you want to bring the animation.
     
     - Returns: `SCNAnimationPlayer` object or `nil`.  If it returns `nil`, please check parameter.
     
     - Since: 1.0.0
     */
    public func loadAnimation(from sceneName: String) -> SCNAnimationPlayer? {
        guard let scene = SCNScene(named: sceneName) else { return nil }
        /// find animation
        var animationPlayer: SCNAnimationPlayer? = nil
        scene.rootNode.enumerateChildNodes { (child, stop) in
            if !child.animationKeys.isEmpty {
                animationPlayer = child.animationPlayer(forKey: child.animationKeys[0])
                stop.pointee = true
            }
        }
        return animationPlayer
    }
    
    public func loadAnimations(for node: SCNNode, with key: String, from sceneName: String) {
        guard let animation = self.loadAnimation(from: sceneName) else { return }
        animation.speed = 1.0
        animation.stop()
        node.addAnimationPlayer(animation, forKey: key)
    }
    
    // MARK: - Model Setup
    public func node(withName name: String, from sceneName: String) -> SCNNode? {
        //***
        guard let nodeScene = SCNScene(named: sceneName),
            let node = nodeScene.rootNode.childNode(withName: name, recursively: false) else {
                return nil
        }
        node.scale = SCNVector3(1, 1, 1)
        node.position = SCNVector3(0, 0, 0)
        // +x: right, +y: up, +x: forward
        return node
    }
    
    
    // MARK: - Animating Model
    public func animate(forKey: String) -> SCNAnimationPlayer? {
        guard let animationPlayer = node!.animationPlayer(forKey: forKey) else { return nil }
        animationPlayer.animation.repeatCount = .infinity
        animationPlayer.animation.blendInDuration = 0.3
        animationPlayer.play()
        
        return animationPlayer
    }
    
    public func animate(with animationPlayer: SCNAnimationPlayer) -> SCNAnimationPlayer {
        animationPlayer.play()
        return animationPlayer
    }
}

