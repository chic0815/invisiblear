//
//  TypeAlias.swift
//  
//
//  Created by 이재성 on 2019/11/16.
//

import ARKit

@available(iOS 11.0, *)
public typealias RendererAction = (_ anchor: ARPlaneAnchor) -> Void

/**
 Action for specific `SCNNode` object.
 
 - parameters:
    - node: `SCNNode`(Scene Node) object.
 */
@available(iOS 11.0, *)
public typealias Action = (_ node: SCNNode) -> Void
