//
//  Renderable.swift
//  
//
//  Created by Jaesung on 2019/11/16.
//

import ARKit

@available(iOS 11.0, *)
protocol Renderable {
    static func render(for anchor: ARAnchor, with action: RendererAction)
    
    static func render(didAdd node: SCNNode, for anchor: ARAnchor, in sceneView: ARSCNView)
    
    static func render(didUpdate node: SCNNode, for anchor: ARAnchor, in sceneView: ARSCNView)
}
