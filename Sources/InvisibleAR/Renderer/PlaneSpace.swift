//
//  PlaneSpace.swift
//  
//
//  Created by Jaesung on 2019/11/16.
//

import ARKit

@available(iOS 11.0, *)
public protocol PlaneSpace {
    func showPlaneSpace(_ anchor: ARPlaneAnchor) -> SCNNode?
}
