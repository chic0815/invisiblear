//
//  ARRenderer.swift
//  
//
//  Created by Jaesung on 2019/11/16.
//

import ARKit

@available(iOS 11.0, *)
public class ARRenderer: Renderable {
    /**
     For specific action with `ARPlaneAnchor`
     
     - parameters:
        - anchor: `ARAnchor` object
        - action: Closure with `ARPlanceAnchor`
     
     - Important: If you want to add guiding node like plane node, use `render(didAdd:for:in:)` method.
     */
    public static func render(for anchor: ARAnchor, with action: RendererAction ) {
        guard let anchor = anchor as? ARPlaneAnchor else { return }
        
        action(anchor)
    }
    
    
    /**
     For adding space node with `ARPlaneAnchor`
     
     - parameters:
        - node: `SCNNode` object.
        - anchor: `ARAnchor` object.
        - sceneView: `ARSCNView` that comforms to `PlaneSpace` protocol.
     
     - Important: If you do not want to comform to `PlaneSpace`, use `render(for:with:)` method rather than this method.
     */
    public static func render(didAdd node: SCNNode, for anchor: ARAnchor, in sceneView: ARSCNView) {
        guard let anchor = anchor as? ARPlaneAnchor else { return }
        guard let sceneView = sceneView as? PlaneSpace else { return }
        guard let spaceNode = sceneView.showPlaneSpace(anchor) else { return }
        
        node.addChildNode(spaceNode)
    }
    
    
    /**
     For updating space node with `ARPlaneAnchor`
         
     - parameters:
        - node: `SCNNode` object.
        - anchor: `ARAnchor` object.
        - sceneView: `ARSCNView` that comforms to `PlaneSpace` protocol.
         
     */
    public static func render(didUpdate node: SCNNode, for anchor: ARAnchor, in sceneView: ARSCNView) {
        guard let anchor = anchor as? ARPlaneAnchor else { return }
        sceneView.scene.rootNode.childNodes { (node, _) -> Bool in
            return node.name == "main"
        }.forEach { node in
            node.removeFromParentNode()
        }
        guard let sceneView = sceneView as? PlaneSpace else { return }
        guard let spaceNode = sceneView.showPlaneSpace(anchor) else { return }
        node.addChildNode(spaceNode)
    }
}



