//
//  IARNode.swift
//  
//
//  Created by Jaesung on 2019/11/16.
//

import ARKit

@available(iOS 11.0, *)
extension SCNNode {
    /**
     Changes the scale of `SCNNode` with a value.
     
     - parameters:
        - value: `CGFloat` value.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func scaling(value: CGFloat) {
        self.scale = SCNVector3(value, value, value)
    }
    
    /**
     Changes the scale of `SCNNode` with `x, y, z`values.
     
     - parameters:
        - x: `CGFloat` value.
        - y: `CGFloat` value.
        - z: `CGFloat` value.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func scaling(x: CGFloat, y: CGFloat, z: CGFloat) {
        self.scale = SCNVector3(x, y, z)
    }
    
    
    /**
     Changes the position of `SCNNode` with `x, y, z` values.
     
     - parameters:
        - x: `CGFloat` value.
        - y: `CGFloat` value.
        - z: `CGFloat` value.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func position(x: CGFloat, y: CGFloat, z: CGFloat) {
        self.position = SCNVector3(x, y, z)
    }
    
    /**
     Allows `SCNNode` object to animate specific animation.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func animate(forKey: String, repeatCount: CGFloat) -> SCNAnimationPlayer {
        let animationPlayer = self.animationPlayer(forKey: forKey)
        animationPlayer?.animation.repeatCount = repeatCount
        animationPlayer?.play()
        
        return animationPlayer!
    }
    
    
    /**
     Allows `SCNNode` object to animate with animation player.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func animate(with player: SCNAnimationPlayer) -> SCNAnimationPlayer {
        player.play()
        
        return player
    }
    
    
    /**
    This method is called when this node was tapped.
     
    - parameters:
       - gesture: `UIPanGestureRecognizer` object
       - action: The action that is applied to this node.
    
    - Examle:
       ```Swift
       @objc func tapGesture(_ gesture: UITapGestureRecognizer) {
           dancingNode.tap(gesture) { node in
               node.action(forKey: "Dance")
           }
       }
       ```
     
     - Since: 1.0.0
     
     - Author: Jaesung
    */
    public func tap(_ gesture: UITapGestureRecognizer, action: Action) {
        guard let view = gesture.view as? ARSCNView else { return }
        let hit = view.hitTest(gesture.location(in: view), options: nil)
        
        guard let target = hit.first?.node, target == self  else { return }
        
        action(self)
    }
    
    
    /**
    This method is called when this node has been panned.
     
    - parameters:
       - gesture: `UIPanGestureRecognizer` object
       - action: The action that is applied to this node. It is `Optional`
    
    - Examle:
       ```Swift
       @objc func panGesture(_ gesture: UIPanGestureRecognizer) {
           dancingNode.pan(gesture, action: nil)
      }
       ```
     
     - Since: 1.0.0

     - Author: Jaesung
    */
    public func move(_ gesture: UIPanGestureRecognizer, action: Action?) {
        gesture.minimumNumberOfTouches = 1
        
        guard let view = gesture.view as? ARSCNView else { return }
        
        let hit = view.hitTest(gesture.location(in: view), options: nil)
        
        guard let target = hit.first?.node, target == self else { return }
        
        let hitWithPan = view.hitTest(gesture.location(in: gesture.view), types: .existingPlaneUsingExtent)
        guard let firstHWP = hitWithPan.first else { return }
        
        let vector = firstHWP.worldTransform.columns.3
        let position = SCNVector3Make(vector.x, vector.y, vector.z)
        
        self.position = position
        
        guard let action = action else { return }
        action(self)
    }
}

