//
//  IARView.swift
//  
//
//  Created by Jaesung on 2019/11/14.
//

import ARKit

@available(iOS 11.0, *)
extension ARSCNView {
    /**
     Creates `Model` object at the `centerVector` with tap gesture.
     
     - parameters:
        - model: `Model` object.
        - centerVector: `SCNVector3`  object.  You have to assign variable property as `SCNVector3`(e.g. self.centerCoordinate)
        - gesture: `UITapGestureRecognizer` object.
     
     - Returns: Optional `SCNVector3` object.  This returned value must be assigned to the `centerVector` property.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func createModel(_ model: Model, at centerVector: SCNVector3?, with gesture: UITapGestureRecognizer) -> SCNVector3? {
        let location = gesture.location(in: self)
        let hit = hitTest(location, options: [:])
        
        guard let first = hit.first else { return nil }
        
        var center = centerVector
        
        if center == nil {
            let hitToCreate = hitTest(location, types: .existingPlaneUsingExtent)
            if let firstHTC = hitToCreate.first {
                scene.addModel(model, result: firstHTC)
                center = SCNVector3(firstHTC.worldTransform.columns.3.x, firstHTC.worldTransform.columns.3.y, firstHTC.worldTransform.columns.3.z)
                
            }
        }
        
        first.node.removeFromParentNode()
        
        return center
    }
    
    
    /**
    Creates `SCNNode`node at the `centerVector` with tap gesture.
    
    - parameters:
       - node: `SCNNode` object.
       - centerVector: `SCNVector3`  object.  You have to assign variable property as `SCNVector3`(e.g. self.centerCoordinate)
       - gesture: `UITapGestureRecognizer` object.
    
    - Returns: Optional `SCNVector3` object.  This returned value must be assigned to the `centerVector` property.
    
    - Since: 1.0.0
    
    - Author: Jaesung
    */
    public func createNode(_ node: SCNNode, at centerVector: SCNVector3?, with gesture: UITapGestureRecognizer) -> SCNVector3? {
        let location = gesture.location(in: self)
        let hit = hitTest(location, options: [:])
        
        guard let first = hit.first else { return nil }
     
        var center = centerVector
        
        if center == nil {
            let hitToCreate = hitTest(location, types: .existingPlaneUsingExtent)
            if let firstHTC = hitToCreate.first {
                scene.addNode(node, result: firstHTC)
                center = SCNVector3(firstHTC.worldTransform.columns.3.x, firstHTC.worldTransform.columns.3.y, firstHTC.worldTransform.columns.3.z)
            }
        }
        
        first.node.removeFromParentNode()
        
        return center
    }
}


