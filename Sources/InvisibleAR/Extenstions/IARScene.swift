//
//  IARScene.swift
//  
//
//  Created by Jaesung on 2019/11/16.
//

import ARKit

@available(iOS 11.0, *)
extension SCNScene {
    /**
     Adds `Model`object to scene with hit test result.
     
     - parameters:
        - hit: `ARHitTestResult` object.
        - model: `Model` object.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func addModel(_ model: Model, result: ARHitTestResult) {
        guard let node = model.node else { return }
        
        let x = result.worldTransform.columns.3.x
        let y = result.worldTransform.columns.3.y
        let z = result.worldTransform.columns.3.z
        
        let position = SCNVector3(x, y, z)
        node.position = position
        
        rootNode.addChildNode(node)
    }
    
    
    /**
     Adds `SCNNode` node to scene with hit test result.
     
     - parameters:
        - hit: `ARHitTestResult` object.
        - node: `SCNNode` object.
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func addNode(_ node: SCNNode, result: ARHitTestResult) {
        let x = result.worldTransform.columns.3.x
        let y = result.worldTransform.columns.3.y
        let z = result.worldTransform.columns.3.z
        
        let position = SCNVector3(x, y, z)
        node.position = position
        
        rootNode.addChildNode(node)
    }
    
    
    /**
     When appropriate space has been found, this method will be called.
     
     - parameters:
        - space: `SCNGeometry` object that you want to use. (e.g. `SCNPlane`)
        - anchor: `ARPlaneAnchor` object.
        - centerVector: `centerVector` property of `ARSCNView`
        - sceneView: `ARSCNView` object.
     
     - Returns: `SCNNode` that contains `space` as a geometry.  If `nil` has been returned, the `centerVector` must be `nil`
     
     - Since: 1.0.0
     
     - Author: Jaesung
     */
    public func findSpace(_ space: SCNGeometry, anchor: ARPlaneAnchor, at centerVector: SCNVector3?, in sceneView: ARSCNView) -> SCNNode? {
        let spaceNode = SCNNode()
        
        guard centerVector == nil else { return nil }
        
        spaceNode.name = "main"
        spaceNode.transform = SCNMatrix4MakeRotation(-.pi / 2, 1.0, 0, 0)
        return spaceNode
    }
}
